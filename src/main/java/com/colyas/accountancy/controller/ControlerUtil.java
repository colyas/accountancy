package com.colyas.accountancy.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ControlerUtil {

	protected static Date getCustomDate(String customDateString) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date customDate = new Date();
		try {
			customDate = format.parse(customDateString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return customDate;
	}

}
