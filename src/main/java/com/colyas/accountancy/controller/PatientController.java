package com.colyas.accountancy.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.colyas.accountancy.dao.PatientDao;
import com.colyas.accountancy.dto.FindPatientDto;
import com.colyas.accountancy.dto.PatientDto;
import com.colyas.accountancy.entity.Patient;

@RestController
@RequestMapping(value = "/patient")
public class PatientController {

	@Autowired
	PatientDao patientDao;

	@PostMapping(path = "/create")
	public Patient createPatient(@RequestBody PatientDto patientDto) {
		Patient patient = Patient.builder().name(patientDto.getName())
				.address(patientDto.getAddress())
				.birthday(ControlerUtil.getCustomDate(patientDto.getBirthday()))
				.build();
		return patientDao.save(patient);
	}
	
	@PutMapping(value = "/update/{id}")
	public ResponseEntity<Patient> updatePatient(@PathVariable("id") long id, @RequestBody PatientDto patientDto) {
		return patientDao.findById(id).map(patient -> {
			((Patient)patient).setName(patientDto.getName());
			patient.setAddress(patientDto.getAddress());
			patient.setBirthday(ControlerUtil.getCustomDate(patientDto.getBirthday()));
			return ResponseEntity.ok().body(patientDao.save(patient));
        }).orElse(ResponseEntity.notFound().build());
	}
	
	@GetMapping(path = "/{id}")
	public FindPatientDto findPatientByidId(@PathVariable("id") long id) throws Exception {
		Patient patient = patientDao.findById(id).orElseThrow(() -> new Exception("Could not find patient " + id));
		return FindPatientDto.builder()
				.name(patient.getName())
				.phoneNumber(patient.getPhoneNumber())
				.address(patient.getAddress())
				.birthday(patient.getBirthday().toString())
				.description(patient.getDescription())
				.email(patient.getEmail())
				.build();
	}
	
	@GetMapping(path = "/all")
	public List<FindPatientDto> findAllPatients() {
		List<Patient> patients = patientDao.findAll();
		List<FindPatientDto> findPatientDtos = new ArrayList<>();
		for(Patient patient : patients) {
			findPatientDtos.add(FindPatientDto.builder()
					.name(patient.getName())
					.phoneNumber(patient.getPhoneNumber())
					.address(patient.getAddress())
					.birthday(patient.getBirthday().toString())
					.description(patient.getDescription())
					.email(patient.getEmail())
					.build());
		}
		return findPatientDtos;
	}
}
