package com.colyas.accountancy.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.colyas.accountancy.dao.ProcedureDao;
import com.colyas.accountancy.dto.FindProcedureDto;
import com.colyas.accountancy.dto.ProcedureDto;
import com.colyas.accountancy.entity.Procedure;

@RestController
@RequestMapping(value = "/procedure")
public class ProcedureControler {

	@Autowired
	ProcedureDao procedureDao;
	
	@PostMapping(path = "/create")
	public Procedure createProcedure(@RequestBody ProcedureDto procedureDto) {
		Procedure procedure = Procedure.builder()
				.procedureName(procedureDto.getProcedureName())
				.price(procedureDto.getPrice())
				.build();
		return procedureDao.save(procedure);
	}
	
	@PutMapping(value = "/update/{id}")
	public ResponseEntity<Procedure> updateProcedure(@PathVariable("id") long id, @RequestBody ProcedureDto procedureDto) {
		return procedureDao.findById(id).map(procedure -> {
			((Procedure)procedure).setProcedureName(procedureDto.getProcedureName());
			procedure.setPrice(procedureDto.getPrice());
			return ResponseEntity.ok().body(procedureDao.save(procedure));
        }).orElse(ResponseEntity.notFound().build());
	}
	
	@DeleteMapping(path = { "/delete/{id}" })
	public ResponseEntity<?> deleteProcedure(@PathVariable long id) {
		return procedureDao.findById(id).map(procedure -> {
			procedureDao.deleteById(id);
			return ResponseEntity.ok().build();
		}).orElse(ResponseEntity.notFound().build());
	}
	
	@GetMapping(path = "/{id}")
	public FindProcedureDto findProcedureByidId(@PathVariable("id") long id) throws Exception {
		Procedure procedure = procedureDao.findById(id).orElseThrow(() -> new Exception("Could not find procedure " + id));
		return FindProcedureDto.builder()
				.procedureName(procedure.getProcedureName())
				.price(procedure.getPrice())
				.build();
	}
	
	@GetMapping(path = "/all")
	public List<FindProcedureDto> findAllProcedures() {
		List<Procedure> procedures = procedureDao.findAll();
		List<FindProcedureDto> findProcedureDtos = new ArrayList<>();
		for(Procedure procedure : procedures) {
			findProcedureDtos.add(FindProcedureDto.builder()
					.procedureName(procedure.getProcedureName())
					.price(procedure.getPrice())
					.build());
		}
		return findProcedureDtos;
	}
}
