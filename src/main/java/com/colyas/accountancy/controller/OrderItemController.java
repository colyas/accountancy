package com.colyas.accountancy.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.colyas.accountancy.dao.OrderDao;
import com.colyas.accountancy.dao.OrderItemDao;
import com.colyas.accountancy.dto.OrderItemDto;
import com.colyas.accountancy.entity.OrderItem;

@RestController
public class OrderItemController {

	@Autowired
	private OrderItemDao orderItemDao;
	
	@Autowired
	private OrderDao orderDao;

	@PostMapping(path = "/order/{orderid}/orderitems")
	public OrderItem createProfit(@PathVariable (value = "orderid") long orderId, @RequestBody OrderItemDto dto) throws Exception {
		OrderItem orderItem = OrderItem.builder()
				.procedureName(dto.getProcedureName())
				.price(Integer.parseInt(dto.getPrice()))
				.build();
		return orderDao.findById(orderId).map(order -> {
			orderItem.setOrder(order);
			return orderItemDao.save(orderItem);
		}).orElseThrow(() -> new Exception("OrderId " + orderId + " not found"));
	}
	
	@DeleteMapping(path = "/order/{orderid}/orderitems/{id}")
	public ResponseEntity<?> deleteOrderItem(@PathVariable (value = "orderid") long orderId, @PathVariable long id) {
		return orderItemDao.findById(id).map(order -> {
			orderItemDao.deleteById(id);
			return ResponseEntity.ok().build();
		}).orElse(ResponseEntity.notFound().build());
	}
	
	@PutMapping(path = "/order/{orderid}/orderitems/{id}")
	public ResponseEntity<OrderItem> updateOrderItem(@PathVariable (value = "orderid") long orderId, @PathVariable("id") long id, @RequestBody OrderItemDto orderItemDto) {
		return orderItemDao.findById(id).map(orderItem -> 
			{((OrderItem)orderItem).setPrice(Integer.parseInt(orderItemDto.getPrice()));
			return ResponseEntity.ok().body(orderItemDao.save(orderItem));
		}).orElse(ResponseEntity.notFound().build());
	}
	
	@GetMapping(path = "/order/{orderid}/orderitems")
	public List<OrderItemDto> getOrderItemsById(@PathVariable (value = "orderid") long orderId) throws Exception {
		List<OrderItem> orderItems = orderItemDao.findByOrderId(orderId);
		List<OrderItemDto> orerItemDtos = new ArrayList<>();
		for(OrderItem orderItem : orderItems) {
			orerItemDtos.add(OrderItemDto.builder()
					.procedureName(orderItem.getProcedureName())
					.price(String.valueOf(orderItem.getPrice()))
					.build());
		}
		return orerItemDtos; 
	}
}
