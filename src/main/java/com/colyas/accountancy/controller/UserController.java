package com.colyas.accountancy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.colyas.accountancy.dao.UserDao;
import com.colyas.accountancy.dto.UserDto;
import com.colyas.accountancy.entity.User;

@RestController
@RequestMapping(value = "/user")
public class UserController {

	@Autowired
	UserDao userDao;

	@PostMapping(path = "/create")
	public User createUser(@RequestBody UserDto userDto) {
		User user = User.builder()
				.userName(userDto.getUserName())
				.password(userDto.getPaswword())
				.build();
		return userDao.save(user);
	}

	@DeleteMapping(path = { "/delete/{id}" })
	public ResponseEntity<?> deleteUser(@PathVariable long id) {
		return userDao.findById(id).map(user -> {
			userDao.deleteById(id);
			return ResponseEntity.ok().build();
		}).orElse(ResponseEntity.notFound().build());
	}

	@PutMapping(path = "/update/{id}")
	public ResponseEntity<User> updateUser(@PathVariable("id") long id, @RequestBody UserDto userDto) {
		return userDao.findById(id).map(user -> {
			((User)user).setUserName(userDto.getUserName());
			user.setPassword(userDto.getPaswword());
			return ResponseEntity.ok().body(userDao.save(user));
        }).orElse(ResponseEntity.notFound().build());
	}
}
