package com.colyas.accountancy.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.colyas.accountancy.dao.OrderDao;
import com.colyas.accountancy.dao.PatientDao;
import com.colyas.accountancy.dao.UserDao;
import com.colyas.accountancy.dto.FindOrderDto;
import com.colyas.accountancy.dto.OrderByRangeDto;
import com.colyas.accountancy.dto.OrderDto;
import com.colyas.accountancy.entity.Order;
import com.colyas.accountancy.entity.Patient;
import com.colyas.accountancy.entity.User;

@RestController
public class OrderController {

	@Autowired
	private OrderDao orderDao;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private PatientDao patientDao;
	
	@PostMapping(path = "/{userid}/{patientId}/orders")
	public Order createOrder(@PathVariable("userid") long userid, @PathVariable("patientId") long patientId, @RequestBody OrderDto orderDto) throws Exception {
		User user = userDao.findById(userid).orElseThrow(() -> new Exception("Could not find user " + userid));
		Patient patient = patientDao.findById(patientId).orElseThrow(() -> new Exception("Could not find patient " + patientId));
		return orderDao.save(Order.builder()
				.bookDate(ControlerUtil.getCustomDate(orderDto.getBookDate()))
				.creationDate(ControlerUtil.getCustomDate(LocalDate.now().toString()))
				.user(user)
				.patient(patient)
				.build());
	}
	
	@DeleteMapping(path = { "/delete/{id}" })
	public ResponseEntity<?> deleteOrder(@PathVariable long id) {
		return orderDao.findById(id).map(order -> {
			orderDao.deleteById(id);
			return ResponseEntity.ok().build();
		}).orElse(ResponseEntity.notFound().build());
	}
	
	@PutMapping(path = "/update/{id}")
	public ResponseEntity<Order> updateOrder(@PathVariable("id") long id, @RequestBody OrderDto orderDto) {
		return orderDao.findById(id).map(order -> {((Order)order)
			.setBookDate(ControlerUtil.getCustomDate(orderDto.getBookDate()));
			return ResponseEntity.ok().body(orderDao.save(order));
		}).orElse(ResponseEntity.notFound().build());
	}
	
	@GetMapping(path = "/{id}")
	public FindOrderDto findOrderByidId(@PathVariable("id") long id) throws Exception {
		Order order = orderDao.findById(id).orElseThrow(() -> new Exception("Could not find order " + id));
		return FindOrderDto.builder()
				.creationDate(order.getCreationDate().toString())
				.bookDate(order.getBookDate().toString())
				.build();
	}
	
	@GetMapping(path = "/all")
	public List<FindOrderDto> findAllOrders() {
		List<Order> orders = orderDao.findAll();
		List<FindOrderDto> findOrderDtos = new ArrayList<>();
		for(Order order : orders) {
			findOrderDtos.add(FindOrderDto.builder()
					.creationDate(order.getCreationDate().toString())
					.bookDate(order.getBookDate().toString())
					.build());
		}
		return findOrderDtos;
	}
	
	@GetMapping(path = "/byrange")
	public List<FindOrderDto> findOrdersByRange(@RequestBody OrderByRangeDto orderByRangeDto) {
		List<Order> orders = orderDao.findByDate(ControlerUtil.getCustomDate(orderByRangeDto.getBeginDate()), ControlerUtil.getCustomDate(orderByRangeDto.getEndDate()));
		List<FindOrderDto> findOrderDtos = new ArrayList<>();
		for(Order order : orders) {
			findOrderDtos.add(FindOrderDto.builder()
					.creationDate(order.getCreationDate().toString())
					.bookDate(order.getBookDate().toString())
					.payedOrder(order.isPayedOrder())
					.build());
		}
		return findOrderDtos;
		
	}
}
