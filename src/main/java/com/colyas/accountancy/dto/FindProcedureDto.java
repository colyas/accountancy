package com.colyas.accountancy.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FindProcedureDto {

	private String procedureName;

	private String price;
}
