package com.colyas.accountancy.dto;

import lombok.Data;

@Data
public class OrderDto {

	private String bookDate;
	
	private String patientId;
}
