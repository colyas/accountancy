package com.colyas.accountancy.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OrderByRangeDto {

	private String beginDate;
	
	private String endDate;
}
