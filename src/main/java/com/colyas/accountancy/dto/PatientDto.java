package com.colyas.accountancy.dto;

import lombok.Data;

@Data
public class PatientDto {

	private String name;

	private String birthday;

	private String address;
	
	private String phoneNumber;
	
	private String email;
	
	private String description;
}
