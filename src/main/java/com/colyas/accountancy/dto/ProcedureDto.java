package com.colyas.accountancy.dto;

import lombok.Data;

@Data
public class ProcedureDto {

	private String procedureName;
	
	private String price;
}
