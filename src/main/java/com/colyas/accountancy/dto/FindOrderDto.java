package com.colyas.accountancy.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FindOrderDto {

	private String bookDate;
	
	private String creationDate;
	
	private boolean payedOrder;
}
