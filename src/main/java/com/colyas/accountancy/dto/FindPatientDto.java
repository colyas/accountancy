package com.colyas.accountancy.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FindPatientDto {

	private String name;

	private String birthday;

	private String address;
	
	private String phoneNumber;
	
	private String email;
	
	private String description;
}
