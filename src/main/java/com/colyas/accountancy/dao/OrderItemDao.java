package com.colyas.accountancy.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.colyas.accountancy.entity.OrderItem;

@Repository
public interface OrderItemDao extends JpaRepository<OrderItem, Long> {

	@Query(value = "select * from order_item as oi where oi.order_id = :orderId", nativeQuery = true)
	List<OrderItem> findByOrderId(@Param("orderId") long orderId);
}
