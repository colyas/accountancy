package com.colyas.accountancy.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.colyas.accountancy.entity.Order;

@Repository
public interface OrderDao extends JpaRepository<Order, Long> {

	@Query(value = "select * from orders as o where o.book_date between :beginDate and :endDate", nativeQuery = true)
	List<Order> findByDate(@Param("beginDate") Date beginDate, @Param("endDate") Date endDate);
}
