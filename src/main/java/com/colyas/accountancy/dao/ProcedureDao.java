package com.colyas.accountancy.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.colyas.accountancy.entity.Procedure;

@Repository
public interface ProcedureDao extends JpaRepository<Procedure, Long> {

}
