package com.colyas.accountancy.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colyas.accountancy.entity.Patient;

public interface PatientDao extends JpaRepository<Patient, Long> {

}
