package com.colyas.accountancy.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Setter
@Getter
@EqualsAndHashCode
@Entity
@Table(name="patients")
public class Patient {

	@Id
	@GeneratedValue
	private Long id;
	
	private String name;

	private Date birthday;

	private String address;
	
	private String phoneNumber;
	
	private String email;
	
	private String description;
	
	private Date cretaionDate;
	
	private boolean deletedPatient;
	
	@OneToMany(mappedBy="patient", fetch = FetchType.LAZY)
	@Cascade(value= {CascadeType.SAVE_UPDATE} )
	@Builder.Default
	private List<Order> orders = new ArrayList<>();
}
