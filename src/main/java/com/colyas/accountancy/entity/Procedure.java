package com.colyas.accountancy.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Setter
@Getter
@EqualsAndHashCode
@Entity
@Table(name="procedures")
public class Procedure {

	@Id
	@GeneratedValue
	private Long id;

	private String procedureName;
	
	private String price;
}
