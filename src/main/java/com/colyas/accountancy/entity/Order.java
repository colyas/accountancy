package com.colyas.accountancy.entity;

import static javax.persistence.CascadeType.PERSIST;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Setter
@Getter
@EqualsAndHashCode
@Entity
@Table(name = "orders")
public class Order {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private Long id;
	
	@Column(name = "book_date")
	private Date bookDate;
	
	@Column(name = "creation_date")
	private Date creationDate;
	
	@Column(name = "payed_order")
	private boolean payedOrder;
	
	@OneToMany(mappedBy="order", fetch = FetchType.LAZY)
	@Cascade(value= {CascadeType.SAVE_UPDATE} )
	@Builder.Default
	private List<OrderItem> orderItems = new ArrayList<>();
	
	@ManyToOne(cascade = PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name="userId", nullable=false)
	@JsonIgnore
	private User user;
	
	@ManyToOne(cascade = PERSIST, fetch = FetchType.LAZY)
    @JoinColumn(name="patientId", nullable=false)
	@JsonIgnore
	private Patient patient;
}
